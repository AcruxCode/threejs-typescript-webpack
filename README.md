![TypeScript and Three.js made easy](https://i.imgur.com/6We17Nm.png)

> **Notice:** This webpack configuration should **not** be used for **production**.

## Features

-   [TypeScript](https://www.typescriptlang.org/) support
-   CSS and HTML support
-   Integrated web server with live reload

## Setup

1. Clone the repository:
    ```
    $ git clone git@gitlab.com:AcruxCode/threejs-typescript-webpack.git
    $ cd threejs-typescript-webpack
    ```
2. Install all required packages:
    ```
    $ npm install
    ```
3. Start the server:
    ```
    $ npm start
    ```
4. Open your web-browser and navigate to [`http://localhost:8080/`](http://localhost:8080/)

## License

License: [MIT](https://opensource.org/licenses/MIT)
