const path = require('path')

//https://webpack.js.org/plugins/html-webpack-plugin/
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
    template: path.resolve(__dirname, 'src', 'index.html'),
    filename: 'index.html',
    inject: 'body'
})

module.exports = {
    mode: 'development',

    entry: path.join(__dirname, 'src', 'main.ts'),
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.ts', '.js']
    },

    plugins: [HTMLWebpackPluginConfig],

    //https://webpack.js.org/configuration/dev-server/
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 8080,
        noInfo: true
    },

    devtool: 'source-map',
    module: {
        rules: [
            //https://www.typescriptlang.org/docs/handbook/react-&-webpack.html (ignore the react part)
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader'
            },
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            },

            //https://webpack.js.org/loaders/css-loader/
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    }
}
